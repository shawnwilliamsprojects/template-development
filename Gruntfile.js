module.exports = function(grunt) {
  'use strict';

  // Force use of Unix newlines
  grunt.util.linefeed = '\n';

  // Project configuration.
  grunt.initConfig({

    clean: {
      dev: 'output/index.html'
    },

    mustache_render: {

      dev: {
        options: {
          directory: 'templates'
        },
        files: [{
          data: 'input.json',
          template: 'templates/core.mustache',
          dest: 'output/email.html'
        }]
      }
    },


    less: {

      dev: {
        options: {
          strictMath: true,
          outputSourceFiles: true
        },
        files: {
          'output/styles/styles.css': 'less/styles.less'
        }
      }
    },

    premailer: {
      dev: {
          options: {},
          files: {
            'output/dist.html': ['output/email.html']
          }
      }
    }
  });

  // These plugins provide necessary tasks.
  require('load-grunt-tasks')(grunt, {
    scope: 'devDependencies'
  });

  grunt.loadNpmTasks('grunt-premailer');
  grunt.loadNpmTasks('grunt-inline-css');

  grunt.registerTask('dev', ['clean:dev', 'mustache_render', 'less:dev', 'premailer:dev']);
};
